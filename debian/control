Source: neutron-lbaas
Section: net
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 10),
 dh-python,
 openstack-pkg-tools,
 po-debconf,
 python3-all,
 python3-pbr (>= 2.0.0),
 python3-setuptools,
 python3-sphinx (>= 1.6.2),
Build-Depends-Indep:
 alembic (>= 0.8.10),
 python3-barbicanclient (>= 4.5.2),
 python3-coverage,
 python3-cryptography (>= 2.1),
 python3-eventlet,
 python3-fixtures,
 python3-hacking,
 python3-keystoneauth1 (>= 3.4.0),
 python3-mock,
 python3-mysqldb,
 python3-netaddr,
 python3-neutron (>= 2:13.0.0~rc1),
 python3-neutron-lib (>= 1.18.0),
 python3-openssl (>= 17.1.0),
 python3-openstackdocstheme (>= 1.18.1),
 python3-oslo.concurrency (>= 3.26.0),
 python3-oslo.config (>= 1:5.2.0),
 python3-oslo.db (>= 4.27.0),
 python3-oslo.i18n (>= 3.15.3),
 python3-oslo.log (>= 3.36.0),
 python3-oslo.messaging (>= 5.29.0),
 python3-oslo.reports (>= 1.18.0),
 python3-oslo.serialization (>= 2.18.0),
 python3-oslo.service (>= 1.24.0),
 python3-oslo.utils (>= 3.33.0),
 python3-oslosphinx,
 python3-oslotest (>= 1:3.2.0),
 python3-psycopg2,
 python3-pyasn1,
 python3-pyasn1-modules,
 python3-pymysql,
 python3-requests (>= 2.14.2),
 python3-requests-mock (>= 1.2.0),
 python3-six,
 python3-sqlalchemy,
 python3-stestr,
 python3-stevedore (>= 1.20.0),
 python3-tempest (>= 1:17.1.0),
 python3-testresources (>= 2.0.0),
 python3-testscenarios,
 python3-testtools (>= 2.2.0),
 python3-webob (>= 1.7.1),
 subunit,
 tempest (>= 1:16.1.0),
Standards-Version: 4.1.1
Vcs-Browser: https://salsa.debian.org/openstack-team/services/neutron-lbaas
Vcs-Git: https://salsa.debian.org/openstack-team/services/neutron-lbaas.git
Homepage: https://github.com/openstack/neutron-lbaas

Package: neutron-lbaas-agent
Architecture: all
Depends:
 adduser,
 debconf,
 lsb-base,
 neutron-common,
 neutron-lbaas-common (= ${source:Version}),
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: Loadbalancer-as-a-Service driver for OpenStack networking - Agent
 Neutron provides an API to dynamically request and configure virtual networks.
 These networks connect "interfaces" from other OpenStack services (such as
 vNICs from Nova VMs). The Neutron API supports extensions to provide advanced
 network capabilities, including QoS, ACLs, and network monitoring.
 .
 This package provides the LBaaS agent.

Package: neutron-lbaas-common
Architecture: all
Depends:
 haproxy,
 neutron-common,
 python3-neutron-lbaas (= ${source:Version}),
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: Loadbalancer-as-a-Service driver for OpenStack networking - common files
 Neutron provides an API to dynamically request and configure virtual networks.
 These networks connect "interfaces" from other OpenStack services (such as
 vNICs from Nova VMs). The Neutron API supports extensions to provide advanced
 network capabilities, including QoS, ACLs, and network monitoring.
 .
 This package contains common files for the Neutron load balancer.

Package: neutron-lbaasv2-agent
Architecture: all
Depends:
 adduser,
 debconf,
 lsb-base,
 neutron-common,
 neutron-lbaas-common (= ${source:Version}),
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: Loadbalancer-as-a-Service driver for OpenStack networking - Agent v2
 Neutron provides an API to dynamically request and configure virtual networks.
 These networks connect "interfaces" from other OpenStack services (such as
 vNICs from Nova VMs). The Neutron API supports extensions to provide advanced
 network capabilities, including QoS, ACLs, and network monitoring.
 .
 This package provides the LBaaS v2 agent.

Package: python3-neutron-lbaas
Architecture: all
Section: python
Depends:
 alembic (>= 0.8.10),
 python3-barbicanclient (>= 4.5.2),
 python3-cryptography (>= 2.1),
 python3-eventlet,
 python3-keystoneauth1 (>= 3.4.0),
 python3-netaddr,
 python3-neutron (>= 2:13.0.0~rc1),
 python3-neutron-lib (>= 1.18.0),
 python3-openssl (>= 17.1.0),
 python3-oslo.config (>= 1:5.2.0),
 python3-oslo.db (>= 4.27.0),
 python3-oslo.i18n (>= 3.15.3),
 python3-oslo.log (>= 3.36.0),
 python3-oslo.messaging (>= 5.29.0),
 python3-oslo.reports (>= 1.18.0),
 python3-oslo.serialization (>= 2.18.0),
 python3-oslo.service (>= 1.24.0),
 python3-oslo.utils (>= 3.33.0),
 python3-pbr (>= 2.0.0),
 python3-pyasn1,
 python3-pyasn1-modules,
 python3-pymysql,
 python3-requests (>= 2.14.2),
 python3-six,
 python3-sqlalchemy,
 python3-stevedore (>= 1.20.0),
 ${misc:Depends},
 ${python3:Depends},
Breaks:
 python-neutron-lbaas,
Replaces:
 python-neutron-lbaas,
Recommends:
 iputils-arping,
Description: Loadbalancer-as-a-Service driver for OpenStack Neutron - Python library
 Neutron provides an API to dynamically request and configure virtual networks.
 These networks connect "interfaces" from other OpenStack services (such as
 vNICs from Nova VMs). The Neutron API supports extensions to provide advanced
 network capabilities, including QoS, ACLs, and network monitoring.
 .
 This package provides the Python library for the Neutron load balancer.
